var epubApp = angular.module("epubApp", [ ]);

epubApp.controller("messageCtrl", ["$scope", "$http", function($scope, $http) {
    
    $scope.userMessages = [ ]; 
    
    $scope.sampleTexts = [
        "Hello World",
        "Happy New Year",
        "Hello Epub"
    ];
    
    // send click events to node server
    $scope.sendMessage = function(messageText) {
        var msgBody = {
            text : messageText,
            user : "abc"
        };
        
        var url = 'http://localhost:3000/';
        $http({ 
            method : "POST",
            url  : url, 
            data : msgBody,
            headers: {'Content-Type': 'application/json'}
        })
        .then(function success(response) {
            console.log(response.data);    
        }, function error(response) {
            console.log(response.data);
        });
    };
    
    // get messages of a user
    $scope.getMessages = function( ) {
        var username = "abc";
        
        // fetch user's messages from node server
        var url = 'http://localhost:3000/' + username;
        
        $http.get(url)
        .then(function success(response) {
            $scope.userMessages = response.data;
            console.log("message count : " + $scope.userMessages.length);
        }, function error(response) {
            console.log(response.data);
        });
    };
    
}]);