var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var Promise = require("bluebird");
var mongoose = Promise.promisifyAll(require("mongoose"));

// connect to mongodb
mongoose.connect("mongodb://localhost:27017/epubmessages");

// app configuration to avoid CORS errors
// * leads to client independent system
app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept, If-Modified-Since");
    res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.setHeader('Access-Control-Allow-Origin', '*');
    next();
});

// express middleware
// app parsing configuration
app.use(bodyParser.json({limit : "50kb"}));
app.use(bodyParser.urlencoded({limit : "50kb", extended:true}));

var http = require("http");

// port number
const port = 3000;
http.createServer(app).listen(port, function() {
    console.log("Node server running on port: 3000");
});

// mongo db models

// options object
var options = {
    timestamps : {
        createdAt : "createdAt",
        updatedAt : "updatedAt"
    }
};

var messageSchema = new mongoose.Schema({
    text : { type : String },
    user : { type : String },
}, options);

var Messages = mongoose.model("Messages", messageSchema);

app.get("/:username", function(req, res, next) {
    Messages.findAsync({
        user : req.params.username
    }).then(function (result) {
        if( result )
            res.json(result)
        else
            res.status(400).send("Invalid Message Id");
    }).catch(function(error) {
       console.log(error.message);
        res.status(500).send("Internal Server Error");
    });
});

app.post("/", function(req, res, next) {
    
    var newMessage = new Messages({
        text : req.body.text,
        user : req.body.user
    });
    
    newMessage.saveAsync()
    .then(function (result) {
        res.json(result);
        console.log("Message recieved: " + req.body.text);      
    })
    .catch(function(error) {
        console.log(error.message);
        res.status(500).send("Internal Server Error");
    });
});